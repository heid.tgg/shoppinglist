context("fill shopping list")


test_that("create new shoppinglist", {
  if (file.exists("test.txt")){
    file.remove("test.txt")
  }
  placeItemIntoShoppingList("bananas", "test.txt")
  data <- vroom::vroom("test.txt", delim = ",", col_names = c("itemName"))
  expect_equal(data$itemName[[1]], "bananas")
  expect_equal(nrow(data), 1)
  if (file.exists("test.txt")){
    file.remove("test.txt")
  }
})

test_that("append to existing shoppinglist", {
  if (file.exists("test.txt")){
    file.remove("test.txt")
  }
  placeItemIntoShoppingList("bananas", "test.txt")
  placeItemIntoShoppingList("apples", "test.txt")
  data <- vroom::vroom("test.txt", delim = ",", col_names = c("itemName"))
  expect_equal(data$itemName[[2]], "apples")
  expect_equal(nrow(data), 2)
  #if (file.exists("test.txt")){
  #  file.remove("test.txt")
  #}
})


